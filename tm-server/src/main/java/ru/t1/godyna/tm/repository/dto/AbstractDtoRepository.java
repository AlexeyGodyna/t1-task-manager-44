package ru.t1.godyna.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.repository.dto.IDtoRepository;
import ru.t1.godyna.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDtoRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    public void set(@NotNull final Collection<M> models) {
        clearAll();
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
